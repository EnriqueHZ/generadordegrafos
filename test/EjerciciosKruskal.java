/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rike
 */

import GeneradorGrafoAleatorio.BarabasiAlbert;
import GeneradorGrafoAleatorio.Erdos_Renyi;
import GeneradorGrafoAleatorio.Geografico;
import GeneradorGrafoAleatorio.Gilbert;


public class EjerciciosKruskal {
    public static void main(String[] args) {
        
        try { 

            int[] nodos = new int[2];   
            nodos[0] = 5;
            nodos[1] = 20;

            for(int numNodos : nodos){
                CrearGrafos(numNodos);
            } 
        }
        catch(Exception ex){
            System.out.println("Error: " + ex.getMessage());
        }        
    }
    
    public static void CrearGrafos(int numNodos){
        
        Geografico geo = new Geografico (numNodos, 0.65f, false, false);
        geo.ConstruyeGrafo();
        geo.AsignarCostos(1, 10);
        geo.EscribeArchivo();
        Geografico kruskalGeografico = new Geografico(geo.Kruskal());
        kruskalGeografico.EscribeArchivo();
        
        
        int aristas;
        if(numNodos <= 10){
            aristas = (int)numNodos*(numNodos-1)/2;
        }else{
            aristas = (int)numNodos*(numNodos-1)/4;
        }
        
        Erdos_Renyi erdos = new Erdos_Renyi (numNodos, aristas, false, false);
        erdos.ConstruyeGrafo();
        erdos.AsignarCostos(1, 10);
        erdos.EscribeArchivo();        
        
        Erdos_Renyi kruskalErdos = new Erdos_Renyi( erdos.Kruskal());   
        kruskalErdos.EscribeArchivo();
            
        
        Gilbert gil = new  Gilbert(numNodos, 0.6f, false, false);
        gil.ConstruyeGrafo();
        gil.AsignarCostos(1, 10);
        gil.EscribeArchivo();
        
        Gilbert kruskalGilbert = new Gilbert(gil.Kruskal());
        kruskalGilbert.EscribeArchivo();       
         
        
        BarabasiAlbert barabasi = new BarabasiAlbert(numNodos,(int)(numNodos/2),false, false);
        barabasi.ConstruyeGrafo();
        barabasi.AsignarCostos(1, 10);
        barabasi.EscribeArchivo();
        
        BarabasiAlbert kruskalBarabasi = new BarabasiAlbert(barabasi.Kruskal());
        kruskalBarabasi.EscribeArchivo();
        
        
    }
}
