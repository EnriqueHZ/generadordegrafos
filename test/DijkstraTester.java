
import GeneradorGrafoAleatorio.Arista;
import GeneradorGrafoAleatorio.Grafo;
import GeneradorGrafoAleatorio.Nodo;
//import GeneradorGrafoAleatorio.Dijkstra;
import GeneradorGrafoAleatorio.Gilbert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rike
 */
public class DijkstraTester {
    static Gilbert gil;
    static    Nodo cero = new Nodo(0, "N_0");
    static    Nodo uno = new Nodo(1, "N_1");
    static    Nodo dos = new Nodo(2, "N_2");
    static   Nodo tres = new Nodo(3, "N_3");
    static    Nodo cuatro = new Nodo(4, "N_4");
    static    Nodo cinco = new Nodo(5, "N_5");
    static    Nodo seis = new Nodo(6, "N_6");
    static    Nodo siete = new Nodo(7, "N_7");
    static    Nodo ocho = new Nodo(8, "N_8");
    public static void main(String[] args) {
        Grafo grafo = new Grafo();        
        
        HashMap<Integer,Nodo> nodos = new HashMap<Integer,Nodo>();
        HashMap<String,Arista> aristas = new HashMap<String,Arista>();
        
        // this._aristas.put(getNombreArista(nodoOrigen, nodoDestino) , new Arista(this._nodos.get(nodoOrigen), 
        // this._nodos.get(nodoDestino)));

        //Arista ceroUno = new Arista(cero.getNombre() +"--" + uno.getNombre(), new Arista( cero, uno));
        Arista ceroSiete= new Arista(cero, siete, 8);
        Arista sieteCero= new Arista(siete, cero, 8);
        
        Arista ceroUno = new Arista(cero, uno, 4);
        Arista unoCero = new Arista(uno, cero, 4);
        
        Arista unoDos =  new Arista(uno,dos, 8);
        Arista dosUno =  new Arista(dos, uno, 8);
        
        Arista unoSiete = new Arista(uno, siete, 11);
        Arista sieteUno = new Arista(siete, uno, 11);
        
        Arista dosTres = new Arista(dos, tres, 7);
        Arista tresDos = new Arista(tres, dos, 7);
        
        
        Arista dosOcho= new Arista(dos, ocho, 2);
        Arista ochoDos= new Arista(ocho, dos, 2);
        
        Arista dosCinco= new Arista(dos, cinco, 4);
        Arista cincoDos= new Arista(cinco, dos, 4);
        
        Arista tresCuatro = new Arista(tres, cuatro, 9);
        Arista cuatroTres = new Arista(cuatro, tres, 9);
        
        Arista tresCinco= new Arista(tres, cinco, 14);
        Arista cincoTres= new Arista(cinco, tres, 14);
        
        Arista cuatroCinco= new Arista(cuatro, cinco, 10);
        Arista cincoCuatro= new Arista(cinco, cuatro, 10);
        
        Arista cincoSeis= new Arista(cinco, seis, 2);
        Arista seisCinco= new Arista(seis, cinco, 2);
        
        Arista seisSiete= new Arista(seis, siete, 1);
        Arista sieteSeis= new Arista(siete, seis, 1);
        
        Arista seisOcho= new Arista(seis, ocho, 6);  
        Arista ochoSeis = new Arista(ocho, seis, 6);  
        
        Arista sieteOcho= new Arista(siete, ocho, 7);
        Arista ochoSiete= new Arista(ocho, siete, 7);
        
        nodos.put(0, cero);
        nodos.put(1, uno);
        nodos.put(2, dos);
        nodos.put(3, tres);
        nodos.put(4, cuatro);
        nodos.put(5, cinco);
        nodos.put(6, seis);
        nodos.put(7, siete);
        nodos.put(8, ocho);

        aristas.put(cero.getNombre() +"--"+ siete.getNombre(), ceroSiete);
        aristas.put(siete.getNombre() +"--"+ cero.getNombre(), sieteCero);
        
        aristas.put(cero.getNombre() +"--"+ uno.getNombre(), ceroUno);
        aristas.put(uno.getNombre() +"--"+ cero.getNombre(), unoCero);
        
        aristas.put(uno.getNombre() +"--"+ dos.getNombre(), unoDos);
        aristas.put(dos.getNombre() +"--"+ uno.getNombre(), dosUno);
        
        aristas.put(uno.getNombre() +"--"+ siete.getNombre(), unoSiete);
        aristas.put(siete.getNombre() +"--"+ uno.getNombre(), sieteUno);
        
        aristas.put(dos.getNombre() +"--"+ tres.getNombre(), dosTres);
        aristas.put(tres.getNombre() +"--"+ dos.getNombre(), tresDos);
        
        aristas.put(dos.getNombre() +"--"+ ocho.getNombre(), dosOcho);
        aristas.put(ocho.getNombre() +"--"+ dos.getNombre(), ochoDos);
        
        aristas.put(dos.getNombre() +"--"+ cinco.getNombre(), dosCinco);
        aristas.put(cinco.getNombre() +"--"+ dos.getNombre(), cincoDos);
        
        aristas.put(tres.getNombre() +"--"+ cuatro.getNombre(), tresCuatro);
        aristas.put(cuatro.getNombre() +"--"+ tres.getNombre(), cuatroTres);
        
        aristas.put(tres.getNombre() +"--"+ cinco.getNombre(), tresCinco);
        aristas.put(cinco.getNombre() +"--"+ tres.getNombre(), cincoTres);
        
        
        aristas.put(cuatro.getNombre() +"--"+ cinco.getNombre(), cuatroCinco);
        aristas.put(cinco.getNombre() +"--"+ cuatro.getNombre(), cincoCuatro);
        
        aristas.put(cinco.getNombre() +"--"+ seis.getNombre(), cincoSeis);
        aristas.put(seis.getNombre() +"--"+ cinco.getNombre(), seisCinco);
        
        aristas.put(seis.getNombre() +"--"+ siete.getNombre(), seisSiete);
        aristas.put(siete.getNombre() +"--"+ seis.getNombre(), sieteSeis);
        
        aristas.put(seis.getNombre() +"--"+ ocho.getNombre(), seisOcho);
        aristas.put(ocho.getNombre() +"--"+ seis.getNombre(), ochoSeis);
        
        aristas.put(siete.getNombre() +"--"+ ocho.getNombre(), sieteOcho);
        aristas.put(ocho.getNombre() +"--"+ siete.getNombre(), ochoSiete);
        
        
        grafo.setNodos(nodos);
        grafo.setAristas(aristas);
        
        
        gil = new Gilbert(grafo);
        CreaListaAdj();
        
        gil.EscribeArchivo();        
        gil.Dijkstra( 0);
        
        
        
        
        
        
        
        
        
        //grafo.Dijkstra(grafo, 0);
        
        
        
        
    }
    
    static void CreaListaAdj(){
        gil._listaAdjacencia = new ArrayList<List<Integer>>();  
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        
        //gil._listaAdjacencia.get(cero.getId()).add(cero.getId());
        gil._listaAdjacencia.get(cero.getId()).add(uno.getId());
        gil._listaAdjacencia.get(cero.getId()).add(siete.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(uno.getId()).add(cero.getId());
        gil._listaAdjacencia.get(uno.getId()).add(dos.getId());
        gil._listaAdjacencia.get(uno.getId()).add(siete.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(dos.getId()).add(uno.getId());
        gil._listaAdjacencia.get(dos.getId()).add(tres.getId());
        gil._listaAdjacencia.get(dos.getId()).add(cinco.getId());
        gil._listaAdjacencia.get(dos.getId()).add(ocho.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(tres.getId()).add(dos.getId());
        gil._listaAdjacencia.get(tres.getId()).add(cuatro.getId());
        gil._listaAdjacencia.get(tres.getId()).add(cinco.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(cuatro.getId()).add(tres.getId());
        gil._listaAdjacencia.get(cuatro.getId()).add(cinco.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());        
        gil._listaAdjacencia.get(cinco.getId()).add(dos.getId());
        gil._listaAdjacencia.get(cinco.getId()).add(tres.getId());
        gil._listaAdjacencia.get(cinco.getId()).add(cuatro.getId());
        gil._listaAdjacencia.get(cinco.getId()).add(seis.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(seis.getId()).add(cinco.getId());
        gil._listaAdjacencia.get(seis.getId()).add(siete.getId());
        gil._listaAdjacencia.get(seis.getId()).add(ocho.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(siete.getId()).add(cero.getId());
        gil._listaAdjacencia.get(siete.getId()).add(uno.getId());
        gil._listaAdjacencia.get(siete.getId()).add(seis.getId());
        gil._listaAdjacencia.get(siete.getId()).add(ocho.getId());
        
        gil._listaAdjacencia.add(new ArrayList<Integer>());
        gil._listaAdjacencia.get(ocho.getId()).add(dos.getId());
        gil._listaAdjacencia.get(ocho.getId()).add(seis.getId());
        gil._listaAdjacencia.get(ocho.getId()).add(siete.getId());
    }
}
