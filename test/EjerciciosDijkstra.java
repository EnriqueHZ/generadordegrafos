
import GeneradorGrafoAleatorio.BFS;
import GeneradorGrafoAleatorio.BarabasiAlbert;
import GeneradorGrafoAleatorio.DFS;
import GeneradorGrafoAleatorio.Erdos_Renyi;
import GeneradorGrafoAleatorio.Geografico;
import GeneradorGrafoAleatorio.Gilbert;
import GeneradorGrafoAleatorio.Grafo;

/*
 * En esta clase se generan todos los grafos y sus rutas minimas de Dijkstra
 */

/**
 *
 * @author rike
 */
public class EjerciciosDijkstra {
    
    public static void main(String[] args) {
        
        try { 

            int[] nodos = new int[2];   
            nodos[0] = 5;
            nodos[1] = 20;

            for(int numNodos : nodos){
                CrearGrafos(numNodos);
            } 
        }
        catch(Exception ex){
            System.out.println("Error: " + ex.getMessage());
        }        
    }
    
    public static void CrearGrafos(int numNodos){
        Geografico geo = new Geografico (numNodos, 0.65f, false, false);
        geo.ConstruyeGrafo();
        geo.AsignarCostos(1, 10);
        geo.EscribeArchivo();
        geo.Dijkstra( 0);
        
        int aristas;
        if(numNodos <= 10){
            aristas = (int)numNodos*(numNodos-1)/2;
        }else{
            aristas = (int)numNodos*(numNodos-1)/4;
        }
        
        
        Erdos_Renyi erdos = new Erdos_Renyi (numNodos, aristas, false, false);
        erdos.ConstruyeGrafo();
        erdos.AsignarCostos(1, 10);
        erdos.EscribeArchivo();
        erdos.Dijkstra( 0);
        
            
        
        Gilbert gil = new  Gilbert(numNodos, 0.6f, false, false);
        gil.ConstruyeGrafo();
        gil.AsignarCostos(1, 10);
        gil.EscribeArchivo();
        gil.Dijkstra( 0);
         
        
        BarabasiAlbert barabasi = new BarabasiAlbert(numNodos,(int)(numNodos/2),false, false);
        barabasi.ConstruyeGrafo();
        barabasi.AsignarCostos(1, 10);
        barabasi.EscribeArchivo();

        barabasi.Dijkstra(0);      
        
        
    }
    
}
