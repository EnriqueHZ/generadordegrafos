/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author rike
 */


package GeneradorGrafoAleatorio;

public class Conjuntos {
	int[] _conjuntos;
	int _arboles;

	public Conjuntos(int elementos) {
		_conjuntos = new int[elementos];
		_arboles = elementos;
		for (int i = 0; i < _conjuntos.length; i++) {
			_conjuntos[i] = -1;
		}
	}

	public void Union(int a, int b) {
		int conjunto_a = Buscar(a);
		int conjunto_b = Buscar(b);
		if (conjunto_a == conjunto_b)
			return;
		if (_conjuntos[conjunto_b] < _conjuntos[conjunto_a]) {
			_conjuntos[conjunto_b] += _conjuntos[conjunto_a];
			_conjuntos[conjunto_a] = conjunto_b;
		} else {
			_conjuntos[conjunto_a] += _conjuntos[conjunto_b];
			_conjuntos[conjunto_b] = conjunto_a;
		}
		_arboles--;
	}

	public int Buscar(int x) {
		if (_conjuntos[x] < 0) {
			return x;
		} else {
			_conjuntos[x] = Buscar(_conjuntos[x]);
			return _conjuntos[x];
		}
	}

	public boolean EsUnion(int a, int b) {
		return Buscar(a) == Buscar(b);
	}

	public int ConjuntoMaximo() {
		int maximo = Integer.MAX_VALUE;

		for (int i = 0; i < _conjuntos.length; i++) {
			maximo = Math.min(maximo, _conjuntos[i]);
		}
		return maximo * -1;
	}
}
