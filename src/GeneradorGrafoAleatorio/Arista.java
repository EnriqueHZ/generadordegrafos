/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneradorGrafoAleatorio;

/**
 *
 * @author rike
 */
import java.util.HashMap;

public class Arista implements Comparable<Arista> {
    private Nodo _origen;
    private Nodo _destino;
    private HashMap<String, Integer> _datos;
    private float peso;
    
    public float GetPeso(){
        return peso;
    }
    
    public void SetPeso(float peso){
        this.peso = peso;
    }
    
    public Arista() {
        this._origen = new Nodo();
        this._destino = new Nodo();
        this._datos = new HashMap<String, Integer>();
    }

    public Arista(Nodo origen, Nodo destino, HashMap datos) {
        this._origen = origen;
        this._destino = destino;
        this._datos = datos;
    }
    
    public Arista(Nodo origen, Nodo destino) {
        this._origen = origen;
        this._destino = destino;
    }
    public Arista(Nodo origen, Nodo destino, float peso) {
        this._origen = origen;
        this._destino = destino;
        this.peso = peso;
    }
    

    public Nodo getOrigen() {
        return _origen;
    }

    public Nodo getDestino() {
        return _destino;
    }

    public HashMap getDatos() {
        return _datos;
    }

    public void setOrigen(Nodo _origen) {
        this._origen = _origen;
    }

    public void setDestino(Nodo _destino) {
        this._destino = _destino;
    }

    public void setDatos(HashMap _datos) {
        this._datos = _datos;
    }
    
    public void AgregarDato(String dato, Integer valor){
        this._datos.put(dato, valor);
    }
    
    @Override
    public int compareTo(Arista o) {
	if (peso - o.GetPeso() > Math.pow(10, -5)) {
            return 1;
	} else if (peso - o.GetPeso() < Math.pow(10, -5)) {
            return -1;
	} else {
            return 0;
            }
	}
       
}
