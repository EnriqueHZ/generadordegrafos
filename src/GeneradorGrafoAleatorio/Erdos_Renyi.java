/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GeneradorGrafoAleatorio;

/**
 *
 * @author rike
 */
import java.util.HashMap;
import java.util.Random;


public class Erdos_Renyi extends Abs_Grafo {   
    
    @Override
    public  Grafo ConstruyeGrafo(){
        
        this.GeneraNodos(this._numNodos);
        this.GeneraAristas();        
        return new Grafo(this._nodos, this._aristas);
    }
    
    public Erdos_Renyi(int n, int m, boolean esDirigido , boolean aceptaCiclos ){
        this._numNodos = n;  
        this.ObtenAristasMaximas(_numNodos);
        if (m > this._paresDeAristas) {
            throw new IllegalArgumentException("El número de aristas debe ser menor que: " + this._paresDeAristas);
        }        
        this._paresDeAristas = m;  
        this._probability = 0.5f;
        this._esDirigido = esDirigido;
        this._aceptaCiclos = aceptaCiclos;
        this._algoritmo = "ErdosRenyi";
    }
    
    public Erdos_Renyi(Grafo g){
        this._nodos = g._nodos;
        this._aristas = g._aristas;
        this._algoritmo = g._algoritmo;
        this._numNodos = CuentaNodos(this._nodos);
    }
    
    
    
    private int CrearVertices(int n){
        return n;
    }
    
    private int CrearNVerticesAleatorios(int valor){
        return (int)(Math.random() * valor);
    }    
    
    
    
    

    
    
    
    
    /*
    @Override
    public String toString(){
        String grafo = this._aristas.toString();
        return "Aun no se llena esta cadena";
    }*/
}
